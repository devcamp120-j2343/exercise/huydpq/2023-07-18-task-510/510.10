const express = require("express");
const { getAllReview, createReview, getReviewByID, updateReview, deleteReview } = require("../controllers/reviewController");

const reviewRouter = express.Router();


reviewRouter.get("/reviews", getAllReview);

reviewRouter.get("/reviews/:reviewId", getReviewByID);

reviewRouter.post("/reviews", createReview);

reviewRouter.put("/reviews/:reviewId", updateReview);

reviewRouter.delete("/reviews/:reviewId", deleteReview);

module.exports = {reviewRouter}