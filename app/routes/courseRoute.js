// khai báo thư viện express
const express = require("express");

// khai báo middleware

const {
    getAllCoures,
    getCoures,
    postCoures,
    putCoures,
    deleteCoures
} = require("../middlewares/courseMiddleware");
const { getAllCourse, getCourseById, createCourse, updateCourse, deleteCourse  } = require("../controllers/courseController");


// tạo ra routes
 const CourseRouter = express.Router();
 
// get all course
 CourseRouter.get("/course", getAllCourse)

 // get a course
 CourseRouter.get("/course/:courseid", getCourseById)

 // Create a Courese
 CourseRouter.post("/course",createCourse)

 // Update a Courese
 CourseRouter.put("/course/:courseid", updateCourse);

 // delete a Course
 CourseRouter.delete("/course/:courseid", deleteCourse)

 module.exports = {CourseRouter};
 